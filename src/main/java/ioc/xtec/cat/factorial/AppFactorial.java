package ioc.xtec.cat.factorial;

import java.util.Scanner;

/**
 * 
 * @author laura (Alejandro Téllez Crespo)
 * @version 1.0.0
 * @since 1.0.0
 */
public class AppFactorial {

    /**
     * Método principal que soloicita al usuario los datos de entrada y executa el algoritmo Factorial sobre el
     * número introducido, además se solicitará que tipo de Factorización se desea hacer si por recursividad o no.
     * Al finalizar el programa podremos ver en consola el resultado.
     * @param args se utilizan los argumentos introducidos en la linea de comandos, en este caso al metodo no se le pasar por parametro.
     */
	public static void main(String[] args) {
		
        @SuppressWarnings("resource")
		Scanner scanner = new Scanner(System.in);
        System.out.print("Alejandro Téllez Crespo!Está muy bien la asignatura, es cierto que es MUY dura debido al formato de las entregas, por lo demás es un lujo y se aprende mucho! \n");
        System.out.print("Introdueix un nombre per calcular el factorial: ");
        int num = scanner.nextInt();

        System.out.print("Escull la forma de càlcul (1 per recursiva, 2 sense recursivitat): ");
        int opcio = scanner.nextInt();

        int resultat;

        switch (opcio) {
            case 1:
                resultat = factorialRecursiu(num);
                break;
            case 2:
                resultat = factorialNoRecursiu(num);
                break;
            default:
                System.out.println("Opció no vàlida. Si us plau, selecciona 1 o 2.");
                return;
        }

        System.out.println("El factorial de " + num + " és: " + resultat);

	}
	
       /**
         * Método que implementa el facotrial recursivo, es decir realiza la factoriazación volviendo a 
         * llamarse a si mismo multiplicando n por n-1 hasta que la propia función entra por el return de n==1
         * y devuelve el resultado total de la factorización
         * @param n --> Número de entrada al cual realizaremos el factorial.
         * @return resultat--> Devuelve el factorial del valor de n.
         */
	public static int factorialRecursiu(int n) {
		
		if (n < 0) {
	        throw new Error("El nombre no pot ser negatiu");
	    }
		
        if (n == 0 || n == 1)
            return 1;
        else
            return n * factorialRecursiu(n - 1);
    }
        
        /**
         * Método que implementa el facotrial no recursivo, es decir lo realiza de forma iterativa 
         * restando 1 a N cada vez que lo multimplica y lo guarda en la variable resultado.
         * @param n --> Número de entrada al cual realizaremos el factorial.
         * @return resultat--> Devuelve el factorial del valor de n.
         */
	
	public static int factorialNoRecursiu(int n) {
		
		if (n < 0) {
	        throw new Error("El nombre no pot ser negatiu");
	    }
		
        int resultat = 1;
        for (int i = 1; i <= n; i++) {
            resultat *= i;
        }
        return resultat;
    }

}
